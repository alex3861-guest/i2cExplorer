/*
This module is a part of "i2cexplorer" programm.
"i2cexplorer" is a small GUI utility working with i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "i2cexplorer.h"
#include "ui_i2cexplorer.h"

i2cExplorer::i2cExplorer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::i2cExplorer)
{
    ui->setupUi(this);
    devScan();
    vScanTimer.setInterval(1000);
    vScanTimer.setSingleShot(true);
    connect(&vScanTimer,SIGNAL(timeout()),this,SLOT(on_ScanTimer()));
}

i2cExplorer::~i2cExplorer()
{
    delete ui;
}

void i2cExplorer::on_pbI2CScan_clicked()
{
    ui->pteTextBox->clear();
//    i2cBusScan();
    vScanTimer.start();
}

void i2cExplorer::i2cBusScan()
{
    int data, deviceNumber = 0;
    for(int i=8;i<120;i++)
    {
        if(vI2cCommunicator.i2cDevice_Connect(i)!=i2cComm::i2c_NoError)
            ui->pteTextBox->appendPlainText("device "+QString().setNum(i)+" connection error...");
        if(vI2cCommunicator.i2cDevice_Read(&data,0)==i2cComm::i2c_NoError)
        {
            ui->pteTextBox->appendPlainText("Responce from address "+QString().setNum(i,16)+",");
            deviceNumber++;
        }
        else
        {
            if(vI2cCommunicator.i2cDevice_Write(&data,0)==i2cComm::i2c_NoError)
            {
                ui->pteTextBox->appendPlainText("Responce from address "+QString().setNum(i,16)+",");
                deviceNumber++;
            }
        }
    }
    ui->pteTextBox->appendPlainText("\n"+QString().setNum(deviceNumber)+" devices found.");
}

void i2cExplorer::devScan()
{
    ui->cbI2cBus->clear();
    QDir dir("/dev","i2c*",QDir::Name,QDir::System);
    ui->cbI2cBus->addItems(dir.entryList());
}

void i2cExplorer::showMessageBox(QString Message)
{
    QMessageBox msgBox;
    msgBox.setText(Message);
    msgBox.exec();
}

// void i2cExplorer::openI2cBus()
// {
//     if(vI2cCommunicator.i2c_SetBus("/dev/"+ui->cbI2cBus->currentText())!= i2cComm::i2c_NoError)
//         showMessageBox("Error!!!\nFailed to open /dev/"+ui->cbI2cBus->currentText());
// }

void i2cExplorer::openI2cBus(QString bus)
{
    if(vI2cCommunicator.i2c_SetBus("/dev/"+bus)!= i2cComm::i2c_NoError)
        showMessageBox("Error!!!\nFailed to open /dev/"+ui->cbI2cBus->currentText());
}

void i2cExplorer::on_cbI2cBus_currentIndexChanged(const QString &arg1)
{
    openI2cBus(arg1);
}

void i2cExplorer::on_ScanTimer()
{
    i2cBusScan();
}

void i2cExplorer::on_pbDeviceRead_clicked()
{
/*    uint8_t *data = new uint8_t[ui->sbBytesNumber->value()];
    vI2cCommunicator.i2cDevice_Connect(ui->sbDeviceAddress->value());
    if(vI2cCommunicator.i2cDevice_Read(data,size_t(ui->sbBytesNumber->value()))!=i2cComm::i2c_NoError)
        ui->pteTextBox->appendPlainText("Device read Error!!!");
    else
    {
        QString s = "";
        for(int i=0;i<ui->sbBytesNumber->value();i++)
            s += QString().asprintf("%02X",data[i]);
        ui->pteTextBox->appendPlainText("Read - "+s);
    }
    delete [] data;
    */

    size_t vbytesNumber = ui->sbBytesNumber->value();
    vI2cCommunicator.i2cDevice_Connect(ui->sbDeviceAddress->value());
    vRData = vI2cCommunicator.i2cDevice_Read(vbytesNumber);
    if(vRData.length() != int(vbytesNumber))
        ui->pteTextBox->appendPlainText("Device read Error!!!");
    else
    {
        QString s = "";
        for(int i=0;i<int(vbytesNumber);i++)
            s += QString().asprintf("%02X",vRData[i]);
        ui->pteTextBox->appendPlainText("Read - "+s);
    }
}

void i2cExplorer::on_pbDeviceWrite_clicked()
{
//    uint8_t *data;
/*
    QString s = ui->leWriteData->text();
    QStringList sList;
    while(s.length()>0)
    {
        sList.append(s.left(2));
        s.remove(0,2);
    }
    int l = sList.length();
    uint8_t *data = new uint8_t[l];
    for(int i=0;i<sList.length();i++)
        data[i] = uint8_t(sList[i].toUInt(nullptr,16));
    if(vI2cCommunicator.i2cDevice_Write(data,size_t(l))!=i2cComm::i2c_NoError)
        ui->pteTextBox->appendPlainText("Device write Error!!!");
    delete [] data;

    //    sList.clear();
*/
    QString s = ui->leWriteData->text();
    QStringList sList;
    while(s.length()>0)
    {
        sList.append(s.left(2));
        s.remove(0,2);
    }
    int l = sList.length();
    vWData.resize(l);

    for(int i=0;i<l;i++)
        vWData[i] = uint8_t(sList[i].toUInt(nullptr,16));
    vI2cCommunicator.i2cDevice_Connect(ui->sbDeviceAddress->value());
    if(vI2cCommunicator.i2cDevice_Write(vWData,size_t(l))!=i2cComm::i2c_NoError)
        ui->pteTextBox->appendPlainText("Device write Error!!!");
}

void i2cExplorer::on_leWriteData_textChanged(const QString &arg1)
{
    QString s = arg1.right(1);
    bool ok;
//    int k = s.toInt(&ok,16);
    s.toInt(&ok,16);
    if(!ok)
    {
        s = arg1;
        s.chop(1);
        ui->leWriteData->setText(s);
    }
}
