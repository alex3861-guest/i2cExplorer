#-------------------------------------------------
#
# Project created by QtCreator 2020-09-09T19:43:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets

TARGET = i2cexplorer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

icon.path = /opt/$${TARGET}
icon.files = i2c_EXP_64.png

INSTALLS += icon

launcher.path = /usr/share/applications
launcher.files = i2cExplorer.desktop

INSTALLS += launcher

docs.path = /opt/$${TARGET}/share/doc
docs.files = docs/*
docs.files += debian/changelog
docs.files += debian/copyright

INSTALLS += docs

CONFIG += c++11

SOURCES += \
        main.cpp \
        i2cexplorer.cpp \
    i2ccomm.cpp

HEADERS += \
        i2cexplorer.h \
    i2ccomm.h

FORMS += \
        i2cexplorer.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    LICENSE \
    README \
    debian/changelog \
    debian/control \
    debian/copyright \
    debian/patches/series \
    debian/rules \
    debian/source/control \
    debian/source/format \
    debian/source/local-options \
    debian/source/options \
    debian/source/patch-header \
    debian/tests/control \
    debian/upstream/metadata \
    debian/watch \
    docs/LICENSE \
    docs/README \
    docs/README \
    i2cExplorer.desktop \
    i2c_EXP_128.png \
    i2c_EXP_64.png
