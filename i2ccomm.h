/*
This module is a part of "i2cexplorer" programm.
"i2cexplorer" is a small GUI utility working with i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef I2CCOMM_H
#define I2CCOMM_H

#include <QObject>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

class i2cComm : public QObject
{
    Q_OBJECT
public:
    enum    DeviceStatus
    {
        NotConnected =0,
        Connected = 1
    };
    enum    i2cErrors
    {
        i2c_NoError = 0,
        i2c_BusIsNotAvailable = -1,
        i2c_DeviceAddressIsOutOfRange = -2,
        i2c_IoctlError = -3,
        i2c_DeviceReadError = -4,
        i2c_DeviceWriteError = -5
    };

    explicit i2cComm(QObject *parent = nullptr);

    i2cErrors i2c_SetBus(QString bus);
    i2cErrors i2cDevice_Connect(int address);
    i2cErrors i2cDevice_Read(void *ReadData, size_t ByteNumber);
    i2cErrors i2cDevice_Write(void *Data, size_t ByteNumber);
    i2cErrors i2cDevice_Write(QVector<uint8_t> Data, size_t ByteNumber);
    QVector<uint8_t> i2cDevice_Read(size_t ByteNumber);
signals:

public slots:

private:
    // Private variables
    QString         i2c_Bus;
    int             i2cFileDescriptor;
    int             i2cDevice_Address;
    DeviceStatus    i2cDevice_Status;
    QVector<uint8_t>    WData;
    QVector<uint8_t>    RData;
};

#endif // I2CCOMM_H
