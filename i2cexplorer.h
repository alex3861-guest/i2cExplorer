/*
This module is a part of "i2cexplorer" programm.
"i2cexplorer" is a small GUI utility working with i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef I2CEXPLORER_H
#define I2CEXPLORER_H

#include <QMainWindow>
#include <QDir>
#include <QStringList>
#include <QMessageBox>
#include <QTimer>

#include "i2ccomm.h"

namespace Ui {
class i2cExplorer;
}

class i2cExplorer : public QMainWindow
{
    Q_OBJECT

public:
    explicit i2cExplorer(QWidget *parent = nullptr);
    ~i2cExplorer();

private slots:
    void on_pbI2CScan_clicked();
    void on_cbI2cBus_currentIndexChanged(const QString &arg1);
    void on_ScanTimer();
    void on_pbDeviceRead_clicked();
    void on_pbDeviceWrite_clicked();
    void on_leWriteData_textChanged(const QString &arg1);

private:
    Ui::i2cExplorer *ui;
    i2cComm         vI2cCommunicator;
    QTimer          vScanTimer;
    QVector<uint8_t> vRData;
    QVector<uint8_t> vWData;

    void i2cBusScan();
    void devScan();
    void showMessageBox(QString Message);
    // void openI2cBus();
    void openI2cBus(QString bus);
};

#endif // I2CEXPLORER_H
