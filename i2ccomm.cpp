/*
This module is a part of "i2cexplorer" programm.
"i2cexplorer" is a small GUI utility working with i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "i2ccomm.h"

i2cComm::i2cComm(QObject *parent) : QObject(parent)
{
    i2cFileDescriptor = 0;
}

i2cComm::i2cErrors i2cComm::i2c_SetBus(QString bus)
{
    i2c_Bus = bus;
    if(i2cFileDescriptor > 0)
        close(i2cFileDescriptor);
    i2cFileDescriptor = open(bus.toStdString().data(), O_RDWR);
    if(i2cFileDescriptor<0)
        return i2c_BusIsNotAvailable;
    else
        return i2c_NoError;
}

i2cComm::i2cErrors i2cComm::i2cDevice_Connect(int address)
{
    i2cDevice_Address = address;
    if(i2cFileDescriptor<=0)
        return i2c_BusIsNotAvailable;
    if ((address > 127) |
        (address < 0))
        return i2c_DeviceAddressIsOutOfRange;

    if(ioctl(i2cFileDescriptor,I2C_SLAVE,address)<0)
        return i2c_IoctlError;
    else
        return i2c_NoError;

    //    vFileName = vI2CFileName.toStdString();
    //    vI2CFile = open(vFileName.data(), O_RDWR);
    //    if (vI2CFile<0)
    //    {
    //        showMessage("Error!!! Probably wrong i2c bus file name.");
    //        return 1;
    //    }
    //    ioctl(vI2CFile,I2C_SLAVE,vI2Caddress);
    //    vI2CRegisterData = 0xFF;
    //    ssize_t vsize = write(vI2CFile, &vI2CRegisterData, 1);
    //    if(vsize!=1)
    //    {
    //        vI2CConnected = false;
    //        emit i2c_connected_changed(false);
    //        emit statusChanged("i2c connection failed");
    //        close(vI2CFile);
    //        return 1;
    //    }
    //    else
    //    {
    //        vI2CConnected = true;
    //        emit i2c_connected_changed(true);
    //        emit statusChanged("i2c connected");
    //        return 0;
    //    }

}

i2cComm::i2cErrors i2cComm::i2cDevice_Read(void *ReadData, size_t ByteNumber)
{
    ssize_t size = read(i2cFileDescriptor, ReadData, ByteNumber);
    if(size < 0)
        return i2c_DeviceReadError;
    else
        return i2c_NoError;
}

i2cComm::i2cErrors i2cComm::i2cDevice_Write(void *Data, size_t ByteNumber)
{
    ssize_t size = write(i2cFileDescriptor, Data, ByteNumber);
    if(size < 0)
        return i2c_DeviceWriteError;
    else
        return i2c_NoError;
}

i2cComm::i2cErrors i2cComm::i2cDevice_Write(QVector<uint8_t> Data, size_t ByteNumber)
{
//    ssize_t size = write(i2cFileDescriptor, Data.data(), ByteNumber);
    uint8_t d;
    d = 0;
    ssize_t size = write(i2cFileDescriptor, &d, 1);
    if(size < 0)
        return i2c_DeviceWriteError;
    else
        return i2c_NoError;
}

QVector<uint8_t> i2cComm::i2cDevice_Read(size_t ByteNumber)
{
    QVector<uint8_t> Data;
    Data.resize(2);
    ssize_t size = read(i2cFileDescriptor, Data.data(), ByteNumber);
    if(size < 0)
        Data.resize(0);
    return Data;
}

